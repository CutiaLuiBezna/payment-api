# -*- coding: utf-8 -*-
from flask import Flask
from .process_payment import process_payment

app = Flask(__name__)

app.config['SECRET_KEY'] = 'nutoateratelesuntlafeldesipar'

app.register_blueprint(process_payment)