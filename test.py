# -*- coding: utf-8 -*-
import requests

cases = ['http://localhost:5000/process_payment/payment?CreditCardNumber=1111111111111111&CardHolder=John Doe&ExpirationDate=2020/4/20&SecurityCode=432&Amount=5',
        'http://localhost:5000/process_payment/payment?CreditCardNumber=1111111111111111&CardHolder=John Doe&ExpirationDate=2020/4/20&SecurityCode=432&Amount=50',
        'http://localhost:5000/process_payment/payment?CreditCardNumber=1111111111111111&CardHolder=John Doe&ExpirationDate=2020/4/20&SecurityCode=432&Amount=501'
        'http://localhost:5000/process_payment/payment?CreditCardNumber=notanumber&CardHolder=John Doe&ExpirationDate=2020/4/20&SecurityCode=432&Amount=500',
        'http://localhost:5000/process_payment/payment?CreditCardNumber=1111111111111111&CardHolder=&ExpirationDate=2020/4/20&SecurityCode=432&Amount=5',
        'http://localhost:5000/process_payment/payment?CreditCardNumber=1111111111111111&CardHolder=John Doe&ExpirationDate=notadate&SecurityCode=432&Amount=5',
        'http://localhost:5000/process_payment/payment?CreditCardNumber=1111111111111111&CardHolder=John Doe&ExpirationDate=2020/4/20&SecurityCode=&Amount=5',
        'http://localhost:5000/process_payment/payment?CreditCardNumber=1111111111111111&CardHolder=John Doe&ExpirationDate=2020/4/20&SecurityCode=notanumber&Amount=5',
        'http://localhost:5000/process_payment/payment?CreditCardNumber=1111111111111111&CardHolder=John Doe&ExpirationDate=2016/4/20&SecurityCode=432&Amount=5']

for i in cases:
    ret = requests.get(i)
    print(ret)