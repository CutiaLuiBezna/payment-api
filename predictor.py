# -*- coding: utf-8 -*-

import pandas as pd
import numpy as np
from sklearn.ensemble import GradientBoostingRegressor
from sklearn.preprocessing import OneHotEncoder
from dateutil.parser import parse as parse_date


class Encoder():
    def prime_encoder(self, priming_data):
        pass

    def encode(self, data):
        pass

    def decode(self, encoded_data):
        pass


class DateEncoder(Encoder):
    def prime_encoder(self, priming_data):
        pass

    def encode(self, data):
        try:
            return int(parse_date(data).timestamp())
        except:
            return None

    def decode(self, encoded_data):
        pass


class StockTickerEncoder(Encoder):
    def prime_encoder(self, priming_data):
        self.ohe = OneHotEncoder(handle_unknown='ignore')
        self.ohe.fit(np.array(priming_data).reshape(-1, 1))

    def encode(self, data):
        return self.ohe.transform(np.array(data).reshape(1, -1)).toarray()[0]

    def decode(self, encoded_data):
        return self.ohe.inverse_transform(encoded_data)


class Predictor():
    def __init__(self):
        self.model = GradientBoostingRegressor()

    def train(self, training_data):
        self.date_encoder = DateEncoder()
        self.stock_encoder = StockTickerEncoder()
        self.stock_encoder.prime_encoder(training_data['stock'])

        training_data['date'] = training_data['date'].apply(self.date_encoder.encode)
        training_data['stock'] = training_data['stock'].apply(self.stock_encoder.encode)

        training_X = []
        for i in range(len(training_data)):
            x = np.concatenate((training_data.iloc[i]['stock'], [training_data.iloc[i]['date']]))
            training_X.append(x)
        training_Y = training_data['Estimated Stock Price']
        self.model.fit(training_X, training_Y)

    def predict(self, stock, date):
        encoded_stock = self.stock_encoder.encode(stock)
        encoded_date = self.date_encoder.encode(date)
        X = [encoded_stock, encoded_date]
        X = np.array(X).reshape(1, -1)
        prediction = self.model.predict(X)
        return prediction


# Interface
def train(predictor):
    raw_df = pd.read_csv(open('dow_jones_index.data', 'r'))
    raw_df['Estimated Stock Price'] = (raw_df['high'].apply(lambda x: float(x.replace('$', ''))) + raw_df['low'].apply(lambda x: float(x.replace('$', '')))) / 2

    training_data = raw_df.drop(columns=list(set(raw_df.columns) - set(['Estimated Stock Price', 'date', 'stock'])))
    predictor.train(training_data)


def predict(predictor, stock, date):
    return predictor.predict(stock, date)


predictor = Predictor()
