# -*- coding: utf-8 -*-
from flask import Blueprint, request
from datetime import datetime

process_payment = Blueprint('process_payment', __name__, url_prefix='/process_payment')


class PaymentGateway:

    def __init__(self, CreditCardNumber, CardHolder, ExpirationDate, SecurityCode, Amount):
        self.CreditCardNumber = CreditCardNumber
        self.CardHolder = CardHolder
        self.ExpirationDate = ExpirationDate
        self.SecurityCode = SecurityCode
        self.Amount = Amount

    def SendPayment(self):
        pass


class CheapPaymentGateway(PaymentGateway):

    def __init__(self, CreditCardNumber, CardHolder, ExpirationDate, SecurityCode, Amount):
        super().__init__(CreditCardNumber, CardHolder, ExpirationDate, SecurityCode, Amount)

    def SendPayment(self):
        return("passed")


class ExpensivePaymentGateway(PaymentGateway):

    def __init__(self, CreditCardNumber, CardHolder, ExpirationDate, SecurityCode, Amount):
        super().__init__(CreditCardNumber, CardHolder, ExpirationDate, SecurityCode, Amount)

    def SendPayment(self):
        return("passed")


class PremiumPaymentGateway(PaymentGateway):

    def __init__(self, CreditCardNumber, CardHolder, ExpirationDate, SecurityCode, Amount):
        super().__init__(CreditCardNumber, CardHolder, ExpirationDate, SecurityCode, Amount)

    def SendPayment(self):
        return("passed")


@process_payment.route('/payment', methods=['GET', 'POST'])
def ProcessPayment():
    if 'CreditCardNumber' in request.args:
        number = request.args['CreditCardNumber']
    else:
        return 'The request is invalid', 400
    if 'CardHolder' in request.args:
        holder = request.args['CardHolder']
    else:
        return 'The request is invalid', 400
    if 'ExpirationDate' in request.args:
        date = request.args['ExpirationDate']
        date = datetime.strptime(date, '%Y/%m/%d')
    else:
        return 'The request is invalid', 400
    if 'SecurityCode' in request.args:
        code = request.args['SecurityCode']
    if 'Amount' in request.args:
        amount = request.args['Amount']
    if amount.isdigit():
        amount = int(amount)
    else:
        return 'The request is invalid', 400
    if not number or not number.isdigit() or len(number) != 16:
        return 'The request is invalid', 400
    if date <= datetime.now():
        return 'The request is invalid', 400
    if not holder and holder is not str():
        return 'The request is invalid', 400
    if code is not str() and not code.isdigit() and len(code) != 3:
        return 'The request is invalid', 400
    if amount < 0:
        return 'The request is invalid', 400
    if amount < 21:
        payment = CheapPaymentGateway(number, date, holder, code, amount)
        if payment.SendPayment() == 'passed':
            return 'Payment is processed', 200
        else:
            return 'Payment not succesful', 500
    if amount < 501:
        payment = ExpensivePaymentGateway(number, date, holder, code, amount)
        if payment.SendPayment() == 'passed':
            return 'Payment is processed', 200
        else:
            payment = CheapPaymentGateway(number, date, holder, code, amount)
            if payment.SendPayment() == 'passed':
                return 'Payment is processed', 200
            else:
                return 'Payment not succesful', 500
    if amount > 500:
        payment = PremiumPaymentGateway(number, date, holder, code, amount)
        for i in range(4):
            success = payment.SendPayment()
            if success == 'passed':
                return 'Payment is processed', 200
            return 'Payment not succesful', 500